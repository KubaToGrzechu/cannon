﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonController : MonoBehaviour {

    [SerializeField] private float _rateOfFire = 2f;

    private bool isWaiting = false;
    private bool isHover = false;
   

    void Start ()
    {
        Debug.Log("working.");
    }
    void OnMouseOver()
    {
        isHover = true;

        if (isWaiting == false)
        {
            StartCoroutine(Example());
        }
       
    }
    private void OnMouseExit()
    {
        isHover = false;
    }

    IEnumerator Example()
    {
        isWaiting = true;
        yield return new WaitForSeconds(3);
        isWaiting = false;

        if(isHover==true)
        {
            FindObjectOfType<BallShooter>().ShootTheBall();
        }
    }

}
